/* Includes */
#include <Arduino.h>
#include <NetworkManager.h>
#include <ESPAsyncWebServer.h>
#include <ArtnetWiFi.h>

#ifdef ESP32
    #include <SparkFunDMX.h>
#endif


/* Project definition */
#define PRJ_NAME "ArtNet DMX Node"
#define PRJ_VER "0.0.1"


/* Main variables */
AsyncWebServer webServer(80);
ArtnetWiFiReceiver artnet;
SparkFunDMX dmx;


void setupNetwork() {
    // Network setup
    networkManager.setHostnamePrefix("ArtNet-");
    networkManager.setApPSK("espartnet");
    networkManager.setApStaticIP(IPAddress(2, 255, 255, 1), IPAddress(2, 255, 255, 1), IPAddress(255, 255, 255, 0));

    // Network start
    networkManager.begin();
    networkManager.start();

    // Connection check
    delay(500);
    // Wait until connected, some parts need to be setup after connection
    if (networkManager.isStaEnabled()) {
        if (WiFi.waitForConnectResult() != WL_CONNECTED) {
            Serial.println(WiFi.waitForConnectResult());
            // TODO use reset counter to retry a few times before fallback
            networkManager.apEnable();
            networkManager.staDisable();
            networkManager.setApTimeout(0);
            networkManager.apStart();
        }
    }

    // Serial connection info
    Serial.printf("Hostname: %s\n", networkManager.getHostname());
    if (networkManager.isApEnabled()) {
        Serial.println("AP active:");
        Serial.printf(" SSID: %s\n", networkManager.getApSSID().c_str());
        Serial.print(" IP Address: ");
        if (networkManager.isApStaticIpEnabled()) {
            Serial.println(networkManager.getApIp());
        } else {
            Serial.println(WiFi.softAPIP());
        }
        if (networkManager.isApTimeoutActive()) {
            Serial.printf(" AP will disable in %ds\n", networkManager.getApTimeout());
        }
    }
    if (networkManager.isStaEnabled()) {
        if (WiFi.isConnected()) {
            Serial.println("WiFi connected:");
            Serial.printf(" SSID: %s\n", networkManager.getStaSSID().c_str());
            Serial.print(" IP Address: ");
            Serial.println(WiFi.localIP());
        } else {
            Serial.println("WiFi did not connect in time!");
            Serial.printf(" SSID: %s\n", networkManager.getStaSSID().c_str());
        }
    }
    
    // Web panel
    webServer.begin();
    networkManager.webInit(&webServer, 80, false);
}

void artnetCallback(const uint8_t *data, const uint16_t size) {
    for (size_t chan = 0; chan < size; chan++) {
        dmx.write(chan + 1, data[chan]);
    }
}

void setup() {
    // Serial setup
    Serial.begin(115200);
    Serial.println();
    Serial.print("Booting ");
    Serial.print(PRJ_NAME);
    Serial.print(" (v");
    Serial.print(PRJ_VER);
    Serial.println(")");
    Serial.flush();

    setupNetwork();

    artnet.subscribe(0, &artnetCallback);
    artnet.begin();

    dmx.initWrite(512);

    // Startup done
    Serial.println("Startup complete!");
    Serial.flush();
}

void loop() {
    networkManager.loop();

    artnet.parse();
    delay(1);

    dmx.update();

    // delay between each loop
    delay(10);
}
